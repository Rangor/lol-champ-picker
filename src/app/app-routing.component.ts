import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChampListComponent } from './champ-list/champ-list.component';
import { ChampComponent } from './champ/champ.component';

const routes: Routes = [
  { path: '', component: ChampListComponent },
  { path: 'champ/:champName', component: ChampComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
