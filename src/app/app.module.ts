import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatToolbarModule,
  MatCardModule,
  MatGridListModule,
  MatButtonModule,
  MatListModule,
  MatButtonToggleModule,
  MatIconModule,
  MatInputModule
} from '@angular/material';

import { AppRoutingModule } from './app-routing.component';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ChampListComponent } from './champ-list/champ-list.component';
import { HttpClientModule } from '@angular/common/http';
import { ChampComponent } from './champ/champ.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ChampListComponent,
    ChampComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatCardModule,
    MatGridListModule,
    MatButtonModule,
    MatListModule,
    MatButtonToggleModule,
    MatInputModule,
    MatIconModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
