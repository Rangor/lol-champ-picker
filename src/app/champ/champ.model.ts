export interface Champion {
  id: number;
  name: string;
  title: string;
  avatar: string;
  roles: string[];
  info: string[];
  stats: string[];
}
