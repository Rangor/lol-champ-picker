import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Champion } from './champ.model';

@Injectable({ providedIn: 'root' })
export class ChampionService {
  champs: Champion[] = [];

  constructor(private httpClient: HttpClient) {
    this.httpClient
      .get<{
        data: {
          champ: {
            id: number;
            key: string;
            name: string;
            title: string;
            tags: string[];
          };
        };
      }>('http://localhost:3000/api/champs')
      .subscribe(champs => {
        const arr = Object.keys(champs.data);
        for (let i = 0; i < arr.length; i++) {
          const champ = {
            id: champs.data[arr[i]].id,
            name: champs.data[arr[i]].name,
            title: champs.data[arr[i]].title,
            avatar:
              'https://ddragon.leagueoflegends.com/cdn/8.14.1/img/champion/' +
              champs.data[arr[i]].key +
              '.png',
            roles: champs.data[arr[i]].tags,
            info: champs.data[arr[i]].info,
            stats: champs.data[arr[i]].stats
          };
          this.champs.push(champ);
        }
      });
  }

  getChampions() {
    return this.champs;
  }

  getChampion(selected_champ: string) {
    const champion = this.champs.find(champ => {
      const champ_name_tolower = champ.name.toLowerCase().replace(/'/g, '');
      const selected_champ_tolower = selected_champ.toLowerCase().replace(/'/g, '');
      return champ_name_tolower === selected_champ_tolower;
    });

    return champion;
  }
}
