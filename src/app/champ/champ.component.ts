import { Component, OnInit } from '@angular/core';
import { Champion } from './champ.model';
import { ChampionService } from './champ.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-champ',
  templateUrl: './champ.component.html',
  styleUrls: ['./champ.component.css']
})
export class ChampComponent implements OnInit {
  champ: Champion;

  constructor(
    public champService: ChampionService,
    public route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('champName')) {
        this.champ = this.champService.getChampion(paramMap.get('champName'));
      }
    });
  }
}
