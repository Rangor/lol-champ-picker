import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChampionService } from '../champ/champ.service';
import { Champion } from '../champ/champ.model';

@Component({
  selector: 'app-champ-list',
  templateUrl: './champ-list.component.html',
  styleUrls: ['./champ-list.component.css']
})
export class ChampListComponent implements OnInit {
  champs: Champion[] = [];
  filtered_champs: Champion[] = [];
  search_term = '';

  constructor(public ChampService: ChampionService) { }

  ngOnInit() {
    this.getChamps();
    this.filtered_champs = this.champs;
  }

  getChamps() {
   this.champs = this.ChampService.getChampions();
  }

  selectRole(selected_role: string) {
    this.filtered_champs = this.champs.filter(champ => {
      if (champ.roles.includes(selected_role)) {
        return champ;
      }
      if (selected_role === 'All') {
        return champ;
      }
    });
  }

  searchChamps() {
    if (this.search_term === '') {
      this.filtered_champs = this.champs;
    } else {
      this.filtered_champs = this.champs.filter(champ => {
        const champ_name = champ.name.toLowerCase().replace(/'/g, '');
        const search_term = this.search_term.toLowerCase().replace(/'/g, '');
        if (champ_name.includes(search_term)) {
          return true;
        }
      });
    }
  }


}
