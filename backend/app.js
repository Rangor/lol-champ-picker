const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const fs = require('fs');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PATCH, PUT, DELETE, OPTIONS'
  );
  next();
});

app.use('/api/champs', (req, res, next) => {
  if (fs.existsSync('./backend/cache/champ-static-data/champs.json')) {
    fs.readFile('./backend/cache/champ-static-data/champs.json', (err, champs) => {
      if (err) throw err;
      res.status(200).json(JSON.parse(champs));
      console.log('Champ data retrieved from file!');
    });
  } else {
    console.log('Error retrieving champs.json from file. Using LOL API to fetch champs.')
    axios('https://na1.api.riotgames.com/lol/static-data/v3/champions?locale=en_US&tags=stats&tags=info&tags=tags&dataById=false&api_key=' + process.env.RIOT_API_KEY)
      .then(response => {
        data = JSON.stringify(response.data);
        fs.writeFile('./backend/cache/champ-static-data/champs.json', data, (err) =>{
          if (err) throw err;
          console.log('Champs.json file has been saved!');
        });
        res.status(200).json(response.data);
    })
    .catch(error => {
      console.log(error);
    });
  }
});

module.exports = app;
